const express = require('express');
let router = express.Router();

router.get('/',(req,res)=>{
  res.end(JSON.stringify({
    "test":"server is up! routes configured"
  }));
});

router.get('/routeWithParams/:testID',(req,res)=>{
  res.end(JSON.stringify(req.params));
});

//The 404 Route
router.get('*', function(req, res){
  res.send('what???', 404);
});
module.exports= router;
