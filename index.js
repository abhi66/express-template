//Module Imports
const express = require('express');
const routeConfig = require('./routing');

let app = new express();

//middlewware
app.use('/',routeConfig);

app.listen(5000);
